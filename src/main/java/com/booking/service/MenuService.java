package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.*;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

public class MenuService {
    private static List<Person> personList = PersonRepository.getAllPerson();
    private static List<Service> serviceList = ServiceRepository.getAllService();
    private static List<Reservation> reservationList = new ArrayList<>();
    private static Scanner input = new Scanner(System.in);

    public static void mainMenu() {
        PrintService printService = new PrintService();
        String[] mainMenuArr = {"Show Data", "Create Reservation", "Complete/cancel reservation", "Exit"};
        String[] subMenuArr = {"Recent Reservation", "Show Customer", "Show Available Employee", "Show History Reservation + Profit", "Back to main menu"};
    
        int optionMainMenu;
        int optionSubMenu;

		boolean backToMainMenu = false;
        boolean backToSubMenu = false;
        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            optionMainMenu = Integer.valueOf(input.nextLine());
            switch (optionMainMenu) {
                case 1:
                    do {
                        printService.printMenu("Show Data", subMenuArr);
                        optionSubMenu = Integer.valueOf(input.nextLine());
                        // Sub menu - menu 1
                        switch (optionSubMenu) {
                            case 1:
                                printService.showRecentReservation(reservationList);
                                break;
                            case 2:
                               printService.showAllCustomer(personList);
                                break;
                            case 3:
                               printService.showAllEmployee(personList);
                                break;
                            case 4:
                                printService.showHistoryReservation(reservationList);
                                break;
                            case 0:
                                backToSubMenu = true;
                        }
                    } while (!backToSubMenu);
                    break;
                case 2:
                    System.out.println("Membuat Reservasi");
                    printService.showAllCustomer(personList);
                    System.out.println("Silahkan masukkan customer ID: ");
                    String customerId = input.nextLine();

                    Customer customer = ReservationService.getCustomerByCustomerId(customerId, personList);

                    if(customer == null) {
                        System.out.println("Pelanggan tidak ditemukan");
                        break;
                    }

                    System.out.println("Silahkan Masukkan Employee Id: ");
                    printService.showAllEmployee(personList);
                    String employeeId = input.nextLine();

                    Employee employee = ReservationService.getEmployeeByCustomerId(employeeId, personList);

                    if(employee == null){
                        System.out.println("Karyawan tidak ditemukan.");
                        break;
                    }

                    System.out.println("Silahkan Masukkan Service Id");
                    printService.showAllServices(serviceList);

                    List<Service> selectedServices = new ArrayList<>();
                    boolean addMoreServices;
                    do {
                        printService.showAllServices(serviceList);
                        String serviceId = input.nextLine();
                        Service service = ReservationService.getServiceByServiceId(serviceId, serviceList);
                        if (service == null) {
                            System.out.println("Layanan tidak ditemukan.");
                        } else {
                            selectedServices.add(service);
                        }
                        System.out.println("Ingin pilih layanan yang lain (Y/T)?");
                        addMoreServices = input.nextLine().equalsIgnoreCase("Y");
                    } while (addMoreServices);

                    Reservation reservation = ReservationService.createReservation(customer, employee, selectedServices);
                    reservationList.add(reservation);

                    System.out.println("Booking Berhasil!");
                    System.out.println("Total Biaya Booking: Rp. " +  reservation.getReservationPrice());
                    break;
                case 3:
                    printService.showRecentReservation(reservationList);
                    System.out.println("Masukkan ID reservasi yang ingin diubah workstagenya:");
                    String reservationIdToEdit = input.nextLine();
                    Reservation reservationToEdit = ReservationService.findReservationById(reservationIdToEdit, reservationList);
                    if (reservationToEdit != null) {
                        System.out.println("Masukkan workstage baru (In Process, Finish, Canceled):");
                        String newWorkstage = input.nextLine();
                        ReservationService.editReservationWorkstage(reservationToEdit, newWorkstage);
                        System.out.println("Workstage berhasil diubah.");
                    } else {
                        System.out.println("Reservasi dengan ID tersebut tidak ditemukan.");
                    }
                    break;

                case 0:
                    backToMainMenu = true;
                    break;
            }
        } while (!backToMainMenu);
		
	}
}
