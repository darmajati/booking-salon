package com.booking.service;

import java.util.List;

import com.booking.models.*;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);
            num++;
        }
    }

    public String printServices(List<Service> serviceList){
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public void showRecentReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+========================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
    }

    public void showAllCustomer(List<Person> personList){
        System.out.println("\nList of All Customers:");
        System.out.printf("%-2s | %-10s | %-10s | %-10s | %-10s | %-10s%n", "No", "ID", "Nama", "Alamat", "Membership", "Uang");
        int counter = 1;
        for(Person person : personList) {
            if (person instanceof Customer) {
                Customer customer = (Customer) person;
                System.out.printf("%-2s | %-10s | %-10s | %-10s | %-10s | %-10s%n", counter++, customer.getId(), customer.getName(), customer.getAddress(), customer.getMember().getMembershipName(), customer.getWallet());
            }

        }
    }

    public void showAllEmployee(List<Person> personList){
        System.out.println("\nList of All Customers:");
        System.out.printf("%-2s | %-10s | %-10s | %-10s | %-10s%n", "No", "ID", "Nama", "Alamat", "Pengalaman");
        int counter = 1;
        for(Person person : personList) {
            if (person instanceof Employee) {
                Employee employee = (Employee) person;
                System.out.printf("%-2s | %-10s | %-10s | %-10s | %-10s%n", counter++, employee.getId(), employee.getName(), employee.getAddress(), employee.getExperience());
            }

        }
    }

    public void showAllServices(List<Service> serviceList) {
        System.out.println("\nList of All Services:");
        System.out.printf("%-12s | %-20s | %-10s%n", "No", "Service ID", "Service Name");
        int count = 1;
        for (Service service : serviceList) {
            System.out.printf("%-12s | %-20s | %-10s%n", count, service.getServiceId(), service.getServiceName());
            count++;
        }
    }

    public void showHistoryReservation(List<Reservation> reservationList) {
        System.out.println("Reservation History:");
        System.out.printf("%-20s | %-15s | %-15s | %-15s | %-15s%n", "Reservation ID", "Customer", "Employee", "Total Price", "Workstage");
        System.out.println("--------------------------------------------------------------------");
        for (Reservation reservation : reservationList) {
            String totalPrice = reservation.getWorkstage().equals("Canceled") ? "Rp. 0.0" : "Rp. " + reservation.getReservationPrice();
            System.out.printf("%-20s | %-15s | %-15s | %-15s | %-15s%n",
                    reservation.getReservationId(),
                    reservation.getCustomer().getName(),
                    reservation.getEmployee().getName(),
                    totalPrice,
                    reservation.getWorkstage());
        }
    }
}
