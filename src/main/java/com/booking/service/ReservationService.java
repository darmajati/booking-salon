package com.booking.service;
import java.util.List;

import com.booking.models.*;


public class ReservationService {
    private static int reservationCounter = 1;
    public static Reservation createReservation(Customer customer, Employee employee, List<Service> services) {
        // Membuat ID reservasi unik
        String reservationId = generateUniqueId();

        double totalReservationPrice = 0;
        for (Service service : services) {
            totalReservationPrice += service.getPrice();
        }

        Reservation reservation = new Reservation(reservationId, customer, employee, services, totalReservationPrice, "In Process");

        double discountedPrice = reservation.calculateReservationPrice();
        reservation.setReservationPrice(discountedPrice);

        return reservation;
    }

    public static Customer getCustomerByCustomerId(String customerId, List<Person> personList) {
        for (Person person : personList) {
            if (person instanceof Customer && person.getId().equals(customerId)) {
                return (Customer) person;
            }
        }
        return null;
    }

    public static Employee getEmployeeByCustomerId(String employeeId, List<Person> personList) {
        for (Person person : personList) {
            if (person instanceof Employee && person.getId().equals(employeeId)) {
                return (Employee) person;
            }
        }
        return null;
    }

    public static Service getServiceByServiceId(String serviceId, List<Service> serviceList) {
        for (Service service : serviceList) {
            if (service.getServiceId().equals(serviceId)) {
                return service;
            }
        }
        return null;
    }



    public static void editReservationWorkstage(Reservation reservation, String newWorkstage) {
        reservation.setWorkstage(newWorkstage);
    }

    private static String generateUniqueId() {
        return "rsv-" + String.format("%02d", reservationCounter++);
    }

    public static Reservation findReservationById(String reservationId, List<Reservation> reservationList) {
        for (Reservation reservation : reservationList) {
            if (reservation.getReservationId().equals(reservationId)) {
                return reservation;
            }
        }
        return null;
    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
